close all; clear; clc;

data = imread('durian.png');

G = data(:, :, 2);
subplot(2,3,1); imshow(G); title('One Color');

diff_imG = imsubtract(G, rgb2gray(data));
subplot(2,3,2); imshow(diff_imG); title('G - Gray');

diff_imG2 = medfilt2(diff_imG, [3 3]);
subplot(2,3,3); imshow(diff_imG2); title('medFilt2');

diff_imG3 = imbinarize(diff_imG2,'adaptive');
subplot(2,3,4); imshow(diff_imG3); title('im2bw');

diff_imG4 = bwareaopen(diff_imG3,300);
subplot(2,3,5); imshow(diff_imG4); title('areaopen');

bwG = bwlabel(diff_imG4, 8);
subplot(2,3,6); imshow(bwG); title('bwG');

se0 = strel('line',5,0);
se90 = strel('line',5,90);
bwLine = imdilate(bwG,[se90 se0]);

bwFill = imfill(bwLine,'holes');
% figure; imshow(bwFill);

bwClose = imerode(bwFill, strel('disk',4));
% figure; imshow(bwClose);

bwClearArea = bwareaopen(bwClose,300);
% figure; imshow(bwClearArea);

bwLine = imdilate(bwClearArea,[se90 se0]);
figure; imshow(bwLine);

statsG = regionprops(bwLine, 'BoundingBox', 'Centroid');

figure; imshow(data);
    
hold on

%This is a loop to bound the red objects in a rectangular box.

for object = 1:length(statsG)
    bbG = statsG(object).BoundingBox;
    bcG = statsG(object).Centroid;
    if mod(object,3)==0
        rectangle('Position',bbG,'EdgeColor','b','LineWidth',2)
        plot(bcG(1),bcG(2), '-m+')
    elseif mod(object,3)==1
        rectangle('Position',bbG,'EdgeColor','magenta','LineWidth',2)
        plot(bcG(1),bcG(2), '-m+')
    else
        rectangle('Position',bbG,'EdgeColor','c','LineWidth',2)
        plot(bcG(1),bcG(2), '-m+')
    end
    num_durian = num2str(object);
    b=text(bcG(1),bcG(2),num_durian);
    set(b, 'FontName', 'Arial', 'FontWeight', 'bold', 'FontSize', 12, 'Color', 'red');
end
f=getframe(gca);
data=frame2im(f);
dataim=im2uint16(data);
name=sprintf('There are %d durians',length(statsG));
title(name);
imwrite(data,'durian_counted.jpg');
hold off
