function varargout = RainInTomorrow(varargin)
% RAININTOMORROW MATLAB code for RainInTomorrow.fig
%      RAININTOMORROW, by itself, creates a new RAININTOMORROW or raises the existing
%      singleton*.
%
%      H = RAININTOMORROW returns the handle to a new RAININTOMORROW or the handle to
%      the existing singleton*.
%
%      RAININTOMORROW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RAININTOMORROW.M with the given input arguments.
%
%      RAININTOMORROW('Property','Value',...) creates a new RAININTOMORROW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before RainInTomorrow_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to RainInTomorrow_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help RainInTomorrow

% Last Modified by GUIDE v2.5 14-Mar-2017 16:50:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @RainInTomorrow_OpeningFcn, ...
                   'gui_OutputFcn',  @RainInTomorrow_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before RainInTomorrow is made visible.
function RainInTomorrow_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to RainInTomorrow (see VARARGIN)

% Choose default command line output for RainInTomorrow
handles.output = hObject;

handles.Url = 'http://api.wunderground.com/api/bca0dd896b585069/forecast10day/q/';
handles.local = 0;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes RainInTomorrow wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = RainInTomorrow_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in FiveDay.
function FiveDay_Callback(hObject, eventdata, handles)
% hObject    handle to FiveDay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.Nonthaburi.Value == 1)
    rainData = webread([handles.Url, 'zmw:00000.309.48456.json']);
elseif(handles.Chanthaburi.Value == 1)
    rainData = webread([handles.Url, 'zmw:00000.1.48480.json']);
elseif(handles.Rayong.Value == 1)
    rainData = webread([handles.Url, 'zmw:00000.401.48478.json']);
end
% rain_data = webread(link_url);
find_Pop = struct2table(rainData.forecast.simpleforecast.forecastday);
Pop = table2array(find_Pop(2:6,9));
Date = zeros(1,5);
Weather = zeros(1,5);
Weather = Pop;

for i = 1:5
    Date(i) = rainData.forecast.simpleforecast.forecastday(i+1).date.day;
end

if(Weather(1) > 60)
    set(handles.Message,'String','Should not water');
else
    set(handles.Message,'String','Should water');
end
plot(Date,Weather);
xlabel('Day');
ylabel('Probability of Precipitation');

% --- Executes on button press in TenYear.
function TenYear_Callback(hObject, eventdata, handles)
% hObject    handle to TenYear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Day = datetime('now','Format','d');


% --- Executes on button press in Nonthaburi.
function Nonthaburi_Callback(hObject, eventdata, handles)
% hObject    handle to Nonthaburi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Chanthaburi.Value = 0;
handles.Rayong.Value = 0;
% Hint: get(hObject,'Value') returns toggle state of Nonthaburi


% --- Executes on button press in Chanthaburi.
function Chanthaburi_Callback(hObject, eventdata, handles)
% hObject    handle to Chanthaburi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Nonthaburi.Value = 0;
handles.Rayong.Value = 0;
% Hint: get(hObject,'Value') returns toggle state of Chanthaburi


% --- Executes on button press in Rayong.
function Rayong_Callback(hObject, eventdata, handles)
% hObject    handle to Rayong (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Chanthaburi.Value = 0;
handles.Nonthaburi.Value = 0;
% Hint: get(hObject,'Value') returns toggle state of Rayong
